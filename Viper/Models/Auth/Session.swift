//
//  Session.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Foundation

struct Session {

    let token: String
}
