//
//  StaticAuthRsource.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit
import Interstellar

struct StaticAuthRsource: AuthResource {

	func login(username: String, password: String, email: String) -> Observable<Result<Session>> {
		return Observable(
			Result { throw UtilityError.networkingError(description: "Test not implemented yet!") }
		)
	}
}
