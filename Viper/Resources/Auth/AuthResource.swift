//
//  AuthResource.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Interstellar

protocol AuthResource {

    func login(username: String, password: String, email: String) -> Observable<Result<Session>>
}
