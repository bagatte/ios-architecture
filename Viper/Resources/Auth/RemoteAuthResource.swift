//
//  RemoteAuthResource.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Interstellar

struct RemoteAuthResource: AuthResource {
    
    let apiEnvironment: APIEnvironment
    let jsonParser: JSONParser = GlossJSONParser()
    let networkingManager: NetworkingManager

    func login(username: String, password: String, email: String) -> Observable<Result<Session>> {
        return networkingManager
            .request(fromEnvironment: apiEnvironment,
                     endpoint: Endpoint(path: "/user",
                                        method: .post,
                                        parameters: ["username": username,
                                                     "password": password,
                                                     "email": email]))
            .then(jsonParser.parse(withKey: nil))
    }
}
