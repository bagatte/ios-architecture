//
//  Resources.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

protocol Resources: class {

    var authResource: AuthResource { get }
}
