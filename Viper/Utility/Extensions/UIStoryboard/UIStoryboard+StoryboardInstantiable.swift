//
//  UIStoryboard+StoryboardInstantiable.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

extension UIStoryboard {

    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    static var authStoryboard: UIStoryboard {
        return UIStoryboard(name: "Auth", bundle: nil)
    }

    /// 
    /// NOTE:
    ///
    /// - param: Generic object that conforms to 'StoryboardInstantiable' protocol
    ///
    /// - returns: Instanciated View Controller based on the Identifier
    ///
    static func instantiateViewControllerOfType<T: StoryboardInstantiable>(t: T.Type) -> T {
        guard let viewController = T.storyboard.instantiateViewController(withIdentifier: T.identifier) as? T else {
            fatalError("Could not instantiate view controller from storyboard.")
        }
        
        return viewController
    }
}
