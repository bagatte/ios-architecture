//
//  AuthViewControllerBuilder.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

struct AuthViewControllerBuilder {

	// MARK: - Private properties

    private let authResource: AuthResource
	private let router: RouterManager

	// MARK: - Init

    init(authResource: AuthResource, router: RouterManager) {
        self.authResource = authResource
		self.router = router
    }

	// MARK: - Public functions

    func onboardViewController() -> OnboardViewController {
        
        let viewController = UIStoryboard.instantiateViewControllerOfType(t: OnboardViewController.self)
        viewController.router = router
        return viewController
    }

    func logInViewController() -> LoginViewController {
    
        let viewController = UIStoryboard.instantiateViewControllerOfType(t: LoginViewController.self)
        viewController.authResource = self.authResource
        
        return viewController
    }
}
