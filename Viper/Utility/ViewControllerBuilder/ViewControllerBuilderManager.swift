//
//  ViewControllerBuilderManager.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

struct ViewControllerBuilderManager {

	// MARK: - Private properties

    private weak var resources: Resources!
	private weak var router: RouterManager!

	// MARK: - Init

    init(resources: Resources, router: RouterManager) {
        self.resources = resources
		self.router = router
    }

	// MARK: - Public properties

    var authViewControllerBuilder: AuthViewControllerBuilder {
		return AuthViewControllerBuilder(authResource: resources.authResource,
										 router: router)
    }
}
