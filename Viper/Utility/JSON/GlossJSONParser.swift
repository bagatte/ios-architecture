//
//  GlossJSONParser.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Gloss

struct GlossJSONParser: JSONParser {

    func parse<T : Model>(withKey key: String?) -> (AnyObject) throws -> T {
        return { (object) in
            
            let json = try self.transformToJSON(object: object)
            return try self.parseJSON(withKey: key)(json)
        }
    }
    
    func parseCollection<T : Model>(withKey key: String?) -> (AnyObject) throws -> [T] {
        return { (object) in
            
            if let key = key {
                let json = try self.transformToJSON(object: object)
                return try self.parseCollection(withKey: key)(json as AnyObject)
            }
            else {
                let jsonArray = try self.transformToJSONArray(object: object)
                return try self.parseCollection()(jsonArray)
            }
        }
    }
    
    func transformToJSON(object: AnyObject) throws -> JSON {
        guard let json = object as? JSON else {
            throw UtilityError.jsonParserInvalidJSON
        }
        
        return json
    }
    
    func transformToJSONArray(object: AnyObject) throws -> [JSON] {
        guard let json = object as? [JSON] else {
            throw UtilityError.jsonParserInvalidJSON
        }
        
        return json
    }
    
    // MARK: - Private functions
    
    private func parseJSON<T: Model>(withKey key: String?) -> (JSON) throws -> T {
        return {
            json in
            
            guard let parsedObject: T = key <~~ json else {
                throw UtilityError.jsonParserDecodingFailure
            }
            
            return parsedObject
        }
    }
    
    private func parseCollection<T: Model>() -> ([JSON]) throws -> [T] {
        return { jsonArray in
            
            let collection = try jsonArray.map { (json) -> T in
                guard let model = T(json: json) else {
                    throw UtilityError.jsonParserDecodingFailure
                }
                
                return model
            }
            
            return collection
        }
        
    }
}

// MARK: - Gloss custom transformations

private func <~~ <T: JSONDecodable>(key: String?, json: JSON) -> T? {
	if let key = key {
		return Decoder.decode(key: key)(json)
	} else {
		return T(json: json)
	}
}
