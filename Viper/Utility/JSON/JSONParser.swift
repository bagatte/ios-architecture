//
//  JSONParser.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

/// JSON type.
typealias JSON = [String: Any]

protocol JSONParser {

    func parse<T: Model>(withKey key: String?) -> (AnyObject) throws -> T
    
    func parseCollection<T: Model>(withKey key: String?) -> (AnyObject) throws -> [T]
    
    func transformToJSON(object: AnyObject) throws -> JSON
    
    func transformToJSONArray(object: AnyObject) throws -> [JSON]
}
