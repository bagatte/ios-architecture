//
//  Auth+GlossDecoding.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Gloss

extension Session: Model {
    
    init?(json: JSON) {
		guard let token: String = "token" <~~ json else {
			return nil
		}
        
        self.token = token
    }
}
