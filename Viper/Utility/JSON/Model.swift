//
//  Model.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Gloss

protocol Model: JSONDecodable, JSONEncodable {

}

extension Model {
    
    func toJSON() -> JSON? {
        return nil
    }
}
