//
//  Identifiable.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

protocol Identifiable {
    
}

extension Identifiable {
    
    /// Returns type name as default identifier.
    static var identifier: String {
        return String(describing: self)
    }
}
