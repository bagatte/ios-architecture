//
//  StoryboardInstantiable.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

protocol StoryboardInstantiable: Identifiable {
    
    static var storyboard: UIStoryboard { get }
}
