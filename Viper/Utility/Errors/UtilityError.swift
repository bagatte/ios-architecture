//
//  UtilityError.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

enum UtilityError: Error {
    
    case jsonFileReadUnsuccessful
    case jsonParserDecodingFailure
    case jsonParserEncodingFailure
    case jsonParserInvalidJSON
    case networkingError(description: String)
}

extension UtilityError: CustomErrorConvertible {
    
    var domain: String {
        
        switch self {
        case .jsonFileReadUnsuccessful:
            return "com.viper.jsonfilereader"
        case .jsonParserDecodingFailure, .jsonParserEncodingFailure, .jsonParserInvalidJSON:
            return "com.viper.jsonparser"
        case .networkingError(_):
            return "com.viper.networking"
        }
    }
    
    var description: String {
        switch self {
        case .jsonFileReadUnsuccessful:
            return "Error reading JSON file."
        case .jsonParserDecodingFailure:
            return "Error decoding JSON."
        case .jsonParserEncodingFailure:
            return "Error encoding JSON."
        case .jsonParserInvalidJSON:
            return "Error transforming JSON into valid format."
        case .networkingError(let description):
            return description
        }
    }
}
