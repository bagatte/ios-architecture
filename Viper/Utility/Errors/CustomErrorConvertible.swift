//
//  CustomErrorConvertible.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

protocol CustomErrorConvertible: CustomStringConvertible {

    var domain: String {get}
}
