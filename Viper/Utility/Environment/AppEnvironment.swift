//
//  AppEnvironment.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

final class AppEnvironment {

	// MARK: - Public properties

	let apiEnvironment: APIEnvironment
	let networkingManager: NetworkingManager
	let routerManager: RouterManager

	// MARK: - Init

	init() {
		apiEnvironment = ViperAPIEnvironment.qa
		networkingManager = AlamofireNetworkingManager()
		routerManager = RouterManager()
	}

	// MARK: - Public functions

	func run(inWindow window: UIWindow?) {
		routerManager.start(withWindow: window,
							viewControllerBuilderManager: ViewControllerBuilderManager(
								resources: self,
								router: routerManager)
		)
	}
}

extension AppEnvironment: Resources {

	var authResource: AuthResource {
		return RemoteAuthResource(apiEnvironment: apiEnvironment,
								  networkingManager: networkingManager)
	}
}
