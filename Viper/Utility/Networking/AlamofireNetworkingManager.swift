//
//  AlamofireNetworkingManager.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Alamofire
import Interstellar

struct AlamofireNetworkingManager {

	// MARK: - Private properties

    private let manager: Alamofire.SessionManager

	// MARK: - Init

    init() {
        let configuration = URLSessionConfiguration.default
        manager = Alamofire.SessionManager(configuration: configuration)
    }
}

// MARK: - Protocol conformance

// MARK: - NetworkingManager

extension AlamofireNetworkingManager: NetworkingManager {

	func request(fromEnvironment environment: APIEnvironment, endpoint: Endpoint)
		-> Observable<Interstellar.Result<AnyObject>> {
			let urlString = environment.baseURL + endpoint.path
			let response = Observable<Interstellar.Result<AnyObject>>()

			manager
				.request(urlString,
						 method: endpoint.method.toAlamofireMethod(),
						 parameters: endpoint.parameters,
						 encoding: URLEncoding.default,
						 headers: environment.headers)
				.responseJSON { (jsonResponse) in

					switch jsonResponse.result {

					case .success(let jsonValue):
						response.update(.success(jsonValue as AnyObject))

					case .failure(let error):
						response.update(.error(UtilityError.networkingError(description: error.localizedDescription)))
					}
			}

			return response
	}
}
