//
//  HTTPMethod.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Alamofire

enum HTTPMethod: String {

    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

extension HTTPMethod {
    
    func toAlamofireMethod() -> Alamofire.HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        case .delete:
            return .delete
        }
    }
}
