//
//  APIEnvironment.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

protocol APIEnvironment {

    var name: String { get }
    var baseURL: String { get }
    var headers: [String: String] { get }
}
