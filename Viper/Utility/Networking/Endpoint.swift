//
//  Endpoint.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

struct Endpoint {

    let path: String
    let method: HTTPMethod
    let parameters: JSON?
    
    init(path: String, method: HTTPMethod, parameters: JSON?) {
        self.path = path
        self.method = method
        self.parameters = parameters
    }
}
