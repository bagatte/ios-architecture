//
//  ViperAPIEnvironment.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

enum ViperAPIEnvironment {

    case qa
    case production
    case staticEnvironment
}

extension ViperAPIEnvironment: APIEnvironment {
    
    var name: String {
        switch self {
        case .qa:
            return "QA"
        case .production:
            return "Production"
        case .staticEnvironment:
            return "Static"
        }
    }
    
    var baseURL: String {
        switch self {
        case .qa:
            return "http://qa.com/api"
        case .production:
            return "http://production.com/api"
        case .staticEnvironment:
            fatalError("No base URL. Fake API environment.")
        }
    }
    
    var headers: [String : String] {
        switch self {
        case .qa, .production:
            return ["X-Requested-With": "XMLHttpRequest"]
        case .staticEnvironment:
            fatalError("No headers. Fake API environment.")
        }
    }
}
