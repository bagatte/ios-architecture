//
//  NetworkingManager.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import Interstellar

protocol NetworkingManager {

    /**
     Makes a network request to a given API environment and endpoint.
     **/
    func request(fromEnvironment environment: APIEnvironment, endpoint: Endpoint) -> Observable<Result<AnyObject>>
}
