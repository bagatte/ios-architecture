//
//  AuthRouter.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

final class AuthRouter {
    
    // MARK: - Private Properties

    private weak var window: UIWindow?
    private let authViewControllerBuilder: AuthViewControllerBuilder
    
    // MARK: - Init

    init(window: UIWindow?, authViewControllerBuilder: AuthViewControllerBuilder) {
        
        self.window = window
        self.authViewControllerBuilder = authViewControllerBuilder
    }
    
    // MARK: - Public Methods

	func routeToLogIn(presentationStyle: PresentationStyle) {
		
        let viewController = authViewControllerBuilder.logInViewController()
        UIViewController.route(viewController, presentationStyle: presentationStyle)
    }
}
