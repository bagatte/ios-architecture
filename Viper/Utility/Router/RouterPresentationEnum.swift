//
//  RouterPresentationEnum.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

enum PresentationStyle {
    case push(navigationController: UINavigationController)
    case present(presenter: UIViewController)
}
