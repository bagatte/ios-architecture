//
//  RouterManager.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

final class RouterManager {

	// MARK: - Private properties

	private weak var window: UIWindow?
	private var viewControllerBuilderManager: ViewControllerBuilderManager?

	// MARK: - Router objects

	private(set) var authRouter: AuthRouter?

	// MARK: - Public functions

	func start(withWindow window: UIWindow?, viewControllerBuilderManager: ViewControllerBuilderManager) {
		self.window = window
		self.viewControllerBuilderManager = viewControllerBuilderManager

		authRouter = AuthRouter(window: window,
								authViewControllerBuilder: viewControllerBuilderManager.authViewControllerBuilder)

		routeToOnboarding()
	}

	// MARK: - Private functions

	private func routeToOnboarding() {
		guard let viewControllerBuilderManager = viewControllerBuilderManager else {
			return
		}

		let onboardViewController: OnboardViewController = viewControllerBuilderManager.authViewControllerBuilder.onboardViewController()
		let navigationController = UINavigationController(rootViewController: onboardViewController)
		replaceRootWindowViewController(with: navigationController)
	}

	private func replaceRootWindowViewController(with viewController: UIViewController) {
		guard let window = window else {
			return
		}

		if let _ = window.rootViewController {
			UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {

				window.rootViewController = viewController

			},completion: nil)
		}
		else {
			window.rootViewController = viewController
		}
	}
}
