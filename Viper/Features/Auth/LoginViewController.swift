//
//  LoginViewController.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {

	// MARK: - Public properties

    var authResource: AuthResource!

	// MARK: - IBActions

    @IBAction func loginTapped(_ sender: AnyObject) {
        
        self.authResource
            .login(username: "username", password: "123456", email: "username@email.com")
			.then { session in
				print("\(session)")
            }
			.error { error in
				guard case UtilityError.networkingError(let description) = error else {
					print("\(error)")
					return
				}
                print("\(description)")
        	}
    }
}

// MARK: - Protocol conformance

// MARK: - StoryboardInstantiable

extension LoginViewController: StoryboardInstantiable {
    
    static var storyboard: UIStoryboard {
        return UIStoryboard.authStoryboard
    }
}

