//
//  OnboardViewController.swift
//  Viper
//
//  Copyright © 2018 Bruno Agatte. All rights reserved.
//

import UIKit

class OnboardViewController: UIViewController {

	// MARK: - Public properties

	var router: RouterManager!

	// MARK: - IBActions

    @IBAction func startButton(_ sender: AnyObject) {
        router
			.authRouter?
			.routeToLogIn(
				presentationStyle: .push(navigationController: navigationController!)
		)
    }
}

// MARK: - Protocol conformance

// MARK: - StoryboardInstantiable

extension OnboardViewController: StoryboardInstantiable {
    
    static var storyboard: UIStoryboard {
        return UIStoryboard.authStoryboard
    }
}
